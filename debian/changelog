streamtuner2 (2.2.2+dfsg-2) unstable; urgency=medium

  * d/patches/0001-fix-python3-incompatible-syntax.patch: Add to fix error

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 19 Mar 2022 18:51:23 +0900

streamtuner2 (2.2.2+dfsg-1) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Archive.
  * d/salsa-ci.yml: Add CI config file.
  * New upstream version 2.2.2+dfsg
  * Bump Standards-Version to 4.6.0
  * d/copyright: Remove a superfluous entry
  * d/control: Change moved file path.
  * d/missing-sources/highlight.js: Add source file for hilight.js

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 19 Mar 2022 17:25:01 +0900

streamtuner2 (2.2.1+dfsg-2) unstable; urgency=medium

  * Remove useless try.py (Closes: #916357)

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 15 Dec 2018 19:30:23 +0900

streamtuner2 (2.2.1+dfsg-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ TANIGUCHI Takaki ]
  * d/watch: Add dfsg suffix
  * New upstream version 2.2.1+dfsg
  * Add python3-distutils to Depends (Closes: #912693, #900351)
  * Bump Stanrads-Version to 4.2.1

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 13 Dec 2018 11:32:23 +0900

streamtuner2 (2.2.0+dfsg-1) unstable; urgency=medium

  * Migrate to Python3
    - Use python3-pil (Closes: #866483)
    - Replace python-keybinder, python-gtk2 and python-glade from Depends
      and add python3-gi to Depends. (Closes: #886198)
  * debian/compat: Update to 11.
  * debian/control: Change Priority to optional.
  * Move Vcs-* to salsa.debian.org.
  * debian/control: Drop dh-exec from B-D.
  * New upstream version 2.2.0+dfsg
  * debian/watch: Add dversionmangle option.
  * debian/rules: Set shebang manually.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 02 Feb 2018 00:40:40 +0900

streamtuner2 (2.2.0-2) unstable; urgency=medium

  * debian/rules: Add exec permission to st2.py.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 02 Feb 2017 20:01:09 +0900

streamtuner2 (2.2.0-1) unstable; urgency=medium

  * debian/ctonrol: update vcs link
    + Bump debian policy to 3.9.8
  * New upstream version 2.2.0
  * debian/install: Skip deleted files.
  * debian/docs: Skip deleted file.
  * debian/menu: Deleted.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 02 Feb 2017 19:34:31 +0900

streamtuner2 (2.1.9-1) unstable; urgency=medium

  * Imported Upstream version 2.1.9
    +  Fix "Find does not work" (Closes: #761475)
  * debian/patches: Remove merged patches..
  * debian/version: Upgrade to 9.
  * debian/rules: Ignore Makefile actions.
  * debian/install: install bundle/*.py. (Closes: #761476)
  * debian/control:
    - Bump Stnadards-Version 3.9.6.
    - Add dh-python to Build-Depends.

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 25 Nov 2015 16:05:28 +0900

streamtuner2 (2.1.3-1) unstable; urgency=medium

  * Imported Upstream version 2.1.3

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 18 Aug 2014 00:02:02 +0900

streamtuner2 (2.1.2-1) unstable; urgency=medium

  * Imported Upstream version 2.1.2

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 09 Aug 2014 19:48:15 +0900

streamtuner2 (2.1.1-1) unstable; urgency=medium

  * Imported Upstream version 2.1.1

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 06 Jun 2014 15:16:33 +0900

streamtuner2 (2.1.0-1) unstable; urgency=low

  * Imported Upstream version 2.1.0
  * debian/patches/00_fixman: Refresh.
  * debian/install: Change UI definition filename.
  * debian/control: Add python-requests to Depend.

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 13 May 2014 22:33:08 +0900

streamtuner2 (2.0.9-2) unstable; urgency=medium

  * Install glade's config file.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 10 Mar 2014 10:03:28 +0900

streamtuner2 (2.0.9-1) unstable; urgency=medium

  * Imported Upstream version 2.0.9
  * Bump Standards-Version to 3.9.5.
  * debian/watch: Change the filename pattern (tgz -> txz).

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 06 Mar 2014 18:39:32 +0900

streamtuner2 (2.0.8-5) unstable; urgency=low

  * debian/control:
    + Add music player packages to Suggests.
    + Change Maintinaer to Debian Multimedia Team.
    + Add python and python-support to Build-Depends. (Closes: #633558)
    + Switch to dh_python2.
  * Bump Standards-Version to 3.9.2

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 15 Jul 2011 11:04:33 +0900

streamtuner2 (2.0.8-4) unstable; urgency=low

  * debian/watch: update URL.
  * debian/copyright: Rewrite in DEP-5 format.
  * debian/control:
    + Add imagemagick to Build-Depends.
    + Update Homepage URL.
  * debian/rules: Install xpm icon.

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 05 Apr 2011 20:41:26 +0900

streamtuner2 (2.0.8-3) unstable; urgency=low

  * debian/control debian/copyright: Oops, revert degrated.

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 29 Jan 2011 13:35:00 +0900

streamtuner2 (2.0.8-2) unstable; urgency=low

  * debian/install: instead of rules target.

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 29 Jan 2011 13:08:44 +0900

streamtuner2 (2.0.8-1) unstable; urgency=low

  * Initial release (Closes: #591452)

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 15 Nov 2010 09:58:12 +0900
